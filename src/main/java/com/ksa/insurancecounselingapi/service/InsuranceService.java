package com.ksa.insurancecounselingapi.service;

import com.ksa.insurancecounselingapi.entity.Insurance;
import com.ksa.insurancecounselingapi.model.InsuranceItem;
import com.ksa.insurancecounselingapi.model.InsuranceRequest;
import com.ksa.insurancecounselingapi.model.InsuranceResponse;
import com.ksa.insurancecounselingapi.repository.InsuranceRepository;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InsuranceService {
    private final InsuranceRepository insuranceRepository;

    public void setInsurance(InsuranceRequest request) {
        Insurance addData = new Insurance();
        addData.setCounselingDay(LocalDate.now());
        addData.setClientName(request.getClientName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAddress(request.getAddress());
        addData.setContract(request.getContract());
        addData.setHopeMoney(request.getHopeMoney());
        addData.setDiseaseCategoryEnum(request.getDiseaseCategoryEnum());
        addData.setDifferentJoin(request.getDifferentJoin());

        insuranceRepository.save(addData);
    }
    public List<InsuranceItem> getInsurances() {
        List<Insurance> originList = insuranceRepository.findAll();
        List<InsuranceItem> result = new LinkedList<>();

        for (Insurance insurance : originList) {
            InsuranceItem addItem = new InsuranceItem();
            addItem.setId(insurance.getId());
            addItem.setClientName(insurance.getClientName());
            addItem.setBirthDay(insurance.getBirthDay());
            addItem.setPhoneNumber(insurance.getPhoneNumber());
            addItem.setContract(insurance.getContract());
            addItem.setHopeMoney(insurance.getHopeMoney());
            addItem.setDiseaseCategoryEnum(insurance.getDiseaseCategoryEnum().getName());
            addItem.setDifferentJoin(insurance.getDifferentJoin());

            result.add(addItem);
        }
        return result;
    }

    public InsuranceResponse getInsurance(long id) {
        Insurance originData = insuranceRepository.findById(id).orElseThrow();

        InsuranceResponse response = new InsuranceResponse();
        response.setId(originData.getId());
        response.setCounselingDay(originData.getCounselingDay());
        response.setClientName(originData.getClientName());
        response.setBirthDay(originData.getBirthDay());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setAddress(originData.getAddress());
        response.setContract(originData.getContract());
        response.setHopeMoney(originData.getHopeMoney());
        response.setDiseaseCategoryEnum(originData.getDiseaseCategoryEnum().getName());
        response.setPossible(originData.getDiseaseCategoryEnum().getPossible() ? "가입가능" : "가입불가");
        response.setDifferentJoin(originData.getDifferentJoin());

        return response;
    }
}
