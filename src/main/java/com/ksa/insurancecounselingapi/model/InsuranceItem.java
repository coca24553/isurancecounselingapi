package com.ksa.insurancecounselingapi.model;

import com.ksa.insurancecounselingapi.enums.DiseaseCategoryEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceItem {
    private Long id;
    private LocalDate counselingDay;
    private String clientName;
    private LocalDate birthDay;
    private String phoneNumber;
    private Boolean contract;
    private Double hopeMoney;
    private String diseaseCategoryEnum;
    private Boolean differentJoin;
}
