package com.ksa.insurancecounselingapi.model;

import com.ksa.insurancecounselingapi.enums.DiseaseCategoryEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceRequest {
    private LocalDate counselingDay;
    private String clientName;
    private LocalDate birthDay;
    private String phoneNumber;
    private String address;
    private Boolean contract;
    private Double hopeMoney;

    @Enumerated(value = EnumType.STRING)
    private DiseaseCategoryEnum diseaseCategoryEnum;

    private Boolean differentJoin;
}
