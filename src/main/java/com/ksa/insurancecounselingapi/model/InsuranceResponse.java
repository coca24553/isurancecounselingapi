package com.ksa.insurancecounselingapi.model;

import com.ksa.insurancecounselingapi.enums.DiseaseCategoryEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceResponse {
    private Long id;
    private LocalDate counselingDay;
    private String clientName;
    private LocalDate birthDay;
    private String phoneNumber;
    private String address;
    private Boolean contract;
    private Double hopeMoney;
    private String diseaseCategoryEnum;
    private String possible;
    private Boolean differentJoin;
}
