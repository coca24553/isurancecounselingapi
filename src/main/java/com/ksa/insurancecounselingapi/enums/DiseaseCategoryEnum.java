package com.ksa.insurancecounselingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DiseaseCategoryEnum {
    CANCER("암", false),
    BRAIN("뇌혈관",false),
    HEART("심장", false),
    ETC("기타", true),
    NONE("질병 없음", true);

    private final String name;
    private final Boolean possible;
}
