package com.ksa.insurancecounselingapi.controller;

import com.ksa.insurancecounselingapi.model.InsuranceItem;
import com.ksa.insurancecounselingapi.model.InsuranceRequest;
import com.ksa.insurancecounselingapi.model.InsuranceResponse;
import com.ksa.insurancecounselingapi.service.InsuranceService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/insurance")
public class InsuranceController {
    private final InsuranceService insuranceService;

    @PostMapping("/new")
    public String setInsurance(@RequestBody InsuranceRequest request) {
        insuranceService.setInsurance(request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<InsuranceItem> getInsurances() {
        return insuranceService.getInsurances();
    }

    @GetMapping("/detail/{id}")
    public InsuranceResponse getInsurance(@PathVariable long id) {
        return insuranceService.getInsurance(id);
    }
}
