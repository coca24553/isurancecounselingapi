package com.ksa.insurancecounselingapi.repository;

import com.ksa.insurancecounselingapi.entity.Insurance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceRepository extends JpaRepository<Insurance, Long> {
}
