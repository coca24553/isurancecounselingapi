package com.ksa.insurancecounselingapi.entity;

import com.ksa.insurancecounselingapi.enums.DiseaseCategoryEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Insurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate counselingDay;

    @Column(nullable = false)
    private String clientName;

    @Column(nullable = false)
    private LocalDate birthDay;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private Boolean contract;

    @Column(nullable = false)
    private Double hopeMoney;

    @Column(nullable = false)
    @Enumerated
    private DiseaseCategoryEnum diseaseCategoryEnum;

    @Column(nullable = false)
    private Boolean differentJoin;
}
